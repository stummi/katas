from setuptools import find_packages, setup

DISTNAME = ‚katas‘
VERSION =‚0.1.0‘
AUTHOR =‚kennst du nicht‘
EMAIL = ‚schreibmir_blossnicht‘
DESCRIPTION = ‚wobe coding dojo katas‘
URL = ‚https://gitlab.com/stummi/katas‘
LICENCE = ‚mach was du willst‘
INSTALL_REQUIRES=[‚itertools‘,‘random‘]
PYTHON_REQUIRE = ‚>=2.7‘

setup(name = DISTNAME,
      version = VERSION,
      description = DESCRIPTION,
      long_description = open(‚README.md‘).read,
      author = AUTHOR,
      author_email = EMAIL,
      licence = LICENCE,
      packages = find_packages(),
      install_requires = INSTALL_REQUIRES,
      python_require = PYTHON_REQUIRE)





