# -*- coding: utf-8 -*-

class TennisGameDefactored1:

    def __init__(self, player1Name, player2Name):
        self.player1Name = player1Name
        self.player2Name = player2Name
        self.p1points = 0
        self.p2points = 0
        
    def won_point(self, playerName):
        if playerName == self.player1Name:
            self.p1points += 1
        else:
            self.p2points += 1
    
    def score(self):
        result = ""
        tempScore=0
        if (self.p1points==self.p2points):
            result = {
                0 : "Love-All",
                1 : "Fifteen-All",
                2 : "Thirty-All",
                3 : "Forty-All",
            }.get(self.p1points, "Deuce")
        elif (self.p1points>=4 or self.p2points>=4):
            minusResult = self.p1points-self.p2points
            if (minusResult==1):
                result ="Advantage " + self.player1Name
            elif (minusResult ==-1):
                result ="Advantage " + self.player2Name
            elif (minusResult>=2):
                result = "Win for " + self.player1Name
            else:
                result ="Win for " + self.player2Name
        else:
            for i in range(1,3):
                if (i==1):
                    tempScore = self.p1points
                else:
                    result+="-"
                    tempScore = self.p2points
                result += {
                    0 : "Love",
                    1 : "Fifteen",
                    2 : "Thirty",
                    3 : "Forty",
                }[tempScore]
        return result


class TennisGameDefactored2:
    def __init__(self, player1Name, player2Name):
        self.player1Name = player1Name
        self.player2Name = player2Name
        self.p1points = 0
        self.p2points = 0
        
    def won_point(self, playerName):
        if playerName == self.player1Name:
            self.P1Score()
        else:
            self.P2Score()
    
    def score(self):
        result = ""
        if (self.p1points == self.p2points and self.p1points < 4):
            if (self.p1points==0):
                result = "Love"
            if (self.p1points==1):
                result = "Fifteen"
            if (self.p1points==2):
                result = "Thirty"
            if (self.p1points==3):
                result = "Forty"
            result += "-All"
        if (self.p1points==self.p2points and self.p1points>3):
            result = "Deuce"
        
        P1res = ""
        P2res = ""
        if (self.p1points > 0 and self.p2points==0):
            if (self.p1points==1):
                P1res = "Fifteen"
            if (self.p1points==2):
                P1res = "Thirty"
            if (self.p1points==3):
                P1res = "Forty"
            
            P2res = "Love"
            result = P1res + "-" + P2res
        if (self.p2points > 0 and self.p1points==0):
            if (self.p2points==1):
                P2res = "Fifteen"
            if (self.p2points==2):
                P2res = "Thirty"
            if (self.p2points==3):
                P2res = "Forty"
            
            P1res = "Love"
            result = P1res + "-" + P2res
        
        
        if (self.p1points>self.p2points and self.p1points < 4):
            if (self.p1points==2):
                P1res="Thirty"
            if (self.p1points==3):
                P1res="Forty"
            if (self.p2points==1):
                P2res="Fifteen"
            if (self.p2points==2):
                P2res="Thirty"
            result = P1res + "-" + P2res
        if (self.p2points>self.p1points and self.p2points < 4):
            if (self.p2points==2):
                P2res="Thirty"
            if (self.p2points==3):
                P2res="Forty"
            if (self.p1points==1):
                P1res="Fifteen"
            if (self.p1points==2):
                P1res="Thirty"
            result = P1res + "-" + P2res
        
        if (self.p1points > self.p2points and self.p2points >= 3):
            result = "Advantage " + self.player1Name
        
        if (self.p2points > self.p1points and self.p1points >= 3):
            result = "Advantage " + self.player2Name
        
        if (self.p1points>=4 and self.p2points>=0 and (self.p1points-self.p2points)>=2):
            result = "Win for " + self.player1Name
        if (self.p2points>=4 and self.p1points>=0 and (self.p2points-self.p1points)>=2):
            result = "Win for " + self.player2Name
        return result
    
    def SetP1Score(self, number):
        for i in range(number):
            self.P1Score()
    
    def SetP2Score(self, number):
        for i in range(number):
            self.P2Score()
    
    def P1Score(self):
        self.p1points +=1
    
    
    def P2Score(self):
        self.p2points +=1
        
class TennisGameDefactored3:
    def __init__(self, player1Name, player2Name):
        self.p1N = player1Name
        self.p2N = player2Name
        self.p1 = 0
        self.p2 = 0
        
    def won_point(self, n):
        if n == self.p1N:
            self.p1 += 1
        else:
            self.p2 += 1
    
    def score(self):
        if (self.p1 < 4 and self.p2 < 4):
            p = ["Love", "Fifteen", "Thirty", "Forty"]
            s = p[self.p1]
            return s + "-All" if (self.p1 == self.p2) else s + "-" + p[self.p2]
        else:
            if (self.p1 == self.p2):
                return "Deuce"
            s = self.p1N if self.p1 > self.p2 else self.p2N
            return "Advantage " + s if ((self.p1-self.p2)*(self.p1-self.p2) == 1) else "Win for " + s

#######
SCORE_STRING = ["Love", "Fifteen", "Thirty", "Forty"]
PLAYER1 = 0
PLAYER2 = 1
WINNING_SCORE = 4

class Messages:
    @staticmethod
    def message(self, messageID):
        if messageID == 'ALL':       return self._message_all()
        if messageID == 'ADVANTAGE': return self._message_advantage()
        if messageID == 'WIN':       return self._message_win()
        if messageID == 'SCORE':     return self._message_score()
        if messageID == 'DEUCE':     return self._message_deuce()
   
    def _message_advantage(self):
       return "Advantage " + self.names[self.leading_player]
    def _message_win(self):
       return "Win for " + self.names[self.leading_player]
    def _message_deuce(self):
       return "Deuce" 
    def _message_all(self):
       return SCORE_STRING[self.scores[PLAYER1]] + "-All" 
    def _message_score(self):
       return SCORE_STRING[self.scores[PLAYER1]] + "-" + SCORE_STRING[self.scores[PLAYER2]]


class MyTennisGame(Messages):
    def __init__(self, name_player1, name_player2):
        self.scores = [0, 0]
        self.names = [name_player1, name_player2]
        self.leading_player = None

    def won_point(self, player_name):
        self.scores[self._player_number(player_name)] += 1

    def score(self):
        if (self.scores[PLAYER1] < WINNING_SCORE) and (self.scores[PLAYER2] < WINNING_SCORE):
            if (self.scores[PLAYER1] == self.scores[PLAYER2]): 
               return self.message(self, 'ALL')
            else: 
               return self.message(self, 'SCORE')
        else:
            if (self.scores[PLAYER1] == self.scores[PLAYER2]):
                return self.message(self, 'DEUCE')
            self.leading_player = PLAYER1 if self.scores[PLAYER1] > self.scores[PLAYER2] else PLAYER2
            if self._one_score_difference():
                return self.message(self, 'ADVANTAGE')
            else:
                return self.message(self, 'WIN')

    def _player_number(self,player_name):
        return self.names.index(player_name)
    
    def _one_score_difference(self):
        return abs(self.scores[PLAYER1] - self.scores[PLAYER2]) == 1
         
   
# NOTE: You must change this to point at the one of the three examples that you're working on!
TennisGame = MyTennisGame