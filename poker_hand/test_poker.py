HANDS = ["KS AS TS QS JS",
        "2H 3H 4H 5H 6H",
        "AS AD AC AH JD",
        "JS JD JC JH 3D",
        "2S AH 2H AS AC",
        "AS 3S 4S 8S 2S",
        "2H 3H 5H 6H 7H",
        "2S 3H 4H 5S 6C",
        "2D AC 3H 4H 5S",
        "AH AC 5H 6H AS",
        "2S 2H 4H 5S 4C",
        "AH AC 5H 6H 7S",
        "AH AC 4H 6H 7S",
        "2S AH 4H 5S KC",
        "2S 3H 6H 7S 9C"]

SUITS = ["SSSSS",
         "HHHHH",
         "SDCHD",
         "SDCHD",
         "SHHSC",
         "SSSSS",
         "HHHHH",
         "SHHSC",
         "DCHHS",
         "HCHHS",
         "SHHSC",
         "HCHHS",
         "HCHHS",
         "SHHSC",
         "SHHSC"]

VALUES = ["KATQJ",
        "23456",
        "AAAAJ",
        "JJJJ3",
        "2A2AA",
        "A3482",
        "23567",
        "23456",
        "2A345",
        "AA56A",
        "22454",
        "AA567",
        "AA467",
        "2A45K",
        "23679"]

SUIT_COUNTS = [1, 1 , 4, 4, 3, 1, 1, 3, 4, 3, 3, 3, 3, 3, 3]

NUMBER_OF_DIFFERENT_CARDS = [5, 5, 2, 2, 2, 5, 5, 5, 5, 3, 3, 4, 4, 5, 5]

import unittest

from pokerhand import suits, values, number_of_suits, number_of_unique_values
from pokerhand import RoyalFlush, Straight, StraightFlush, OnePair, TwoPairs, ThreeOfAKind, FourOfAKind, FullHouse, HighCard
from pokerhand import PokerHand

class PokerTest(unittest.TestCase):
    
    def test_suits_of_hand(self):
        test_suits = list(map (suits, HANDS) )                
        self.assertEquals(test_suits, SUITS)
    
    def test_values_of_hand(self):
        test_values = list(map (values, HANDS) )                
        self.assertEquals(test_values, VALUES)
    
    def test_suits_counts_of_hand(self):
        test_suits_counts = list(map (number_of_suits, HANDS) )                
        self.assertEquals(test_suits_counts, SUIT_COUNTS)

    def test_count_different_cards_in_hand(self):
        test_no_different_cards = list(map (number_of_unique_values, HANDS) )                
        self.assertEquals(test_no_different_cards, NUMBER_OF_DIFFERENT_CARDS)

    def test_royal_flush(self):
        hands_true = RoyalFlush(HANDS[0])
        hands_false = RoyalFlush(HANDS[1])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 1)

    def test_straight_flush(self):
        hands_true = StraightFlush(HANDS[1])
        hands_false = StraightFlush(HANDS[6])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 2)
   
    def test_one_pair(self):
        hands_true = OnePair(HANDS[11])
        hands_false = OnePair(HANDS[7])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 9)
 

    def test_two_pairs(self):
        hands_true = TwoPairs(HANDS[10])
        hands_false = TwoPairs(HANDS[7])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 8)
    

    def test_full_house(self):
        hands_true = FullHouse(HANDS[4])
        hands_false = FullHouse(HANDS[6])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 4)
    
    def test_straight(self):
        hands_true = Straight(HANDS[7])
        hands_false = Straight(HANDS[1])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 6)

    def test_three_of_a_kind(self):
        hands_true = ThreeOfAKind(HANDS[9])
        hands_false = ThreeOfAKind(HANDS[1])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 7)

    def test_four_of_a_kind(self):
        hands_true = FourOfAKind(HANDS[3])
        hands_false = FourOfAKind(HANDS[1])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 3)

    def test_highcard(self):
        hands_true = HighCard(HANDS[14])
        hands_true2 = HighCard(HANDS[13])
        hands_false = HighCard(HANDS[1])
        self.assertEquals(hands_true.match_found, True)
        self.assertEquals(hands_true2.match_found, True)
        self.assertEquals(hands_false.match_found, False)
        self.assertEquals(hands_true.rank, 10)
        self.assertLess(hands_true.high_card, hands_true2.high_card)
     
    def test_sorting(self):
        test_hands_sort = list(map(PokerHand, HANDS))
        test_hands_sort.sort()
        test_hands_sorted = sorted(list(map(PokerHand, HANDS)))

        for ihand in range(len(HANDS)):
            self.assertEquals(test_hands_sorted[ihand], test_hands_sort[ihand])

    def test_sorting_as_in_readme(self):        
            hands = []
            hands.append(PokerHand("KS 2H 5C JD TD"))
            hands.append(PokerHand("2C 3C AC 4C 5C"))
            hands.append(PokerHand("5C 4D 3C 2S AS"))
            hands.sort()
            sorted_hands = ["2C 3C AC 4C 5C", "5C 4D 3C 2S AS", "KS 2H 5C JD TD"]

            for ihand in range(len(hands)):
                  self.assertEquals(str(hands[ihand]), sorted_hands[ihand])

    def test_ranking_highcard(self):
        hands = []
        hands.append(PokerHand("AS 2H 4C 3D 5D"))
        hands.append(PokerHand("QS JH KC TD 9D"))
        hands.append(PokerHand("QS JH KC TD 8D"))
        hands.sort()
        # expect hand with ace as winner
        self.assertEqual(hands[0], PokerHand("AS 2H 4C 3D 5D"))
        # expect next the hand with the 9
        self.assertLess(hands[1], PokerHand("QS JH KC TD 8D"))

    def test_ranking_onepair(self):
        hands = []
        hands.append(PokerHand("AS AH 4C 3D 5D"))
        hands.append(PokerHand("3S 3H 2C 9D 7D"))
        hands.append(PokerHand("2S 2H KC TD QD"))
        hands.append(PokerHand("3S 3H KC TD QD"))
        hands.sort()
        # expect ace pair as winner
        self.assertEqual(hands[0], PokerHand("AS AH 4C 3D 5D"))
        # expect next threes with King
        self.assertLess(hands[1], PokerHand("3S 3H 2C 9D 7D"))
        # expext now the other threes 
        self.assertLess(hands[2], PokerHand("2S 2H KC TD QD"))
    
    def test_ranking_twopairs(self):
        hands=[]
        hands.append(PokerHand("AS AH KC KH TD QD"))
        hands.append(PokerHand("2S 2H 3C 3H AD 6D"))
        hands.append(PokerHand("2S 2H 3S 3D KD 4D"))
        hands.sort()
        # expect ace pairs as winner
        self.assertEqual(hands[0], PokerHand("AS AH KC KH TD QD"))
        # expect next the one this the ace as high card 
        self.assertLess(hands[1], PokerHand("2S 2H 3S 3D KD 4D"))
    
    def test_ranking_threeofakind(self):
        hands=[]
        hands.append(PokerHand("AS AH AC 2H 3D"))
        hands.append(PokerHand("KS KH KC 4H 6D"))
        hands.sort()
        self.assertLess(hands[0], PokerHand("KS KH KC 4H 6D"))
    
    def test_ranking_fullhouse(self):
        hands=[]
        hands.append(PokerHand("AS AH AC KH KD"))
        hands.append(PokerHand("QS QH QC 4H 4D"))
        hands.sort()
        self.assertLess(hands[0], PokerHand("QS QH QC 4H 4D"))

if __name__ == '__main__':
    unittest.main()
