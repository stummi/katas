from poker_constants import *
from poker_commons import *
from functools        import total_ordering as autocomplete_ordering


class RoyalFlush(object):
    '''5 cards of one suit with the 5 highest values
    RANK 1 (no high card)'''
    def __init__(self, hand):
        self.rank = RANK_ROYAL_FLUSH
        self.high_card = ZERO
        self.hand = hand

    @ property
    def match_found(self):
       return (number_of_suits(self.hand)== SUITS_FLUSH) and is_royal(self.hand) 
    
class StraightFlush(object):
    '''Straight with 5 cards of one suit (highest card wins)
    RANK 2'''
    def __init__(self, hand):
        self.rank = RANK_STRAIGHT_FLUSH
        self.high_card = highcard_weighted_card_value_sum(hand)
        self.hand = hand

    @ property
    def match_found(self):
        return is_straight(self.hand) and (number_of_suits(self.hand) == SUITS_FLUSH)

class FourOfAKind(object):
    '''4 cards of one kind (highest card wins)
    RANK 3'''
    def __init__(self, hand):
        self.rank = RANK_FOUR_OF_A_KIND
        self.high_card = high_card_multiple_of_a_kind(hand, FOUR_OF_A_KIND)
        self.hand = hand
    
    @property
    def match_found(self):
        if number_of_unique_values(self.hand) == VALUES_FOUR_OF_A_KIND:
            return is_multiple_cards_of_one_kind(self.hand, FOUR_OF_A_KIND)
        return False
  
class FullHouse(object):
    '''a pair plus three of a kind (highest three of a kind wins)
    RANK 4'''
    def __init__(self, hand):
        self.rank = RANK_FULL_HOUSE
        self.high_card = high_card_multiple_of_a_kind(hand, THREE_OF_A_KIND)
        self.hand = hand
    
    @ property
    def match_found(self):
        if number_of_unique_values(self.hand) == VALUES_FULL_HOUSE:
            is_pair = is_multiple_cards_of_one_kind(self.hand, PAIR)
            is_three_of_a_kind = is_multiple_cards_of_one_kind(self.hand, THREE_OF_A_KIND)
            return is_three_of_a_kind and is_pair
        return False    
   
class Flush():
    '''5 cards of one suit (recursive win of card values)
    RANK 5'''
    def __init__(self, hand):
        self.rank = RANK_FLUSH
        self.high_card = highcard_weighted_card_value_sum(hand)
        self.hand = hand 
    
    @property
    def match_found(self):
         return (number_of_suits(self.hand) == SUITS_FLUSH) and not(is_royal(self.hand)) and not(is_straight(self.hand))

class Straight(object):   
    '''5 cards of one suit (recursive win of card values)
    RANK 6'''
    def __init__(self, hand):
        self.rank = RANK_STRAIGHT
        self.high_card = highcard_weighted_card_value_sum(hand)
        self.hand = hand 

    @property
    def match_found(self):
        return is_straight(self.hand) and not(number_of_suits(self.hand) == SUITS_FLUSH)
        
class ThreeOfAKind(object):
    ''' 3 cards of one kind highest card in three of a kind wins 
    RANK 7'''
    def __init__(self, hand):
        self.rank = RANK_THREE_OF_A_KIND
        self.high_card = high_card_multiple_of_a_kind(hand, THREE_OF_A_KIND)
        self.hand = hand 

    @property    
    def match_found(self):
        if number_of_unique_values(self.hand) == VALUES_THREE_OF_A_KIND:
            is_three_of_a_kind = is_multiple_cards_of_one_kind(self.hand, THREE_OF_A_KIND)
            is_pair = is_multiple_cards_of_one_kind(self.hand, PAIR) 
            return is_three_of_a_kind and not(is_pair)
        return False     

class TwoPairs(object):     
    ''' highest pairs and highest high card wins
    RANK 8'''
    def __init__(self, hand):
        self.rank = RANK_TWO_PAIRS
        self.high_card = high_card_pairs(hand)
        self.hand = hand 

    @property
    def match_found(self):
        return self._number_of_pairs(self.hand) == TWO_PAIRS

    def _number_of_pairs(self, hand):
        number_of_pairs = ZERO
        for value in sorted_value_string(hand):
           if sorted_value_string(hand).count(value) == PAIR :
               number_of_pairs += 0.5
        return int(number_of_pairs)
    
class OnePair(object):     
    ''' Highest pair plus highest recursive high card wins
    RANK 9'''
    def __init__(self, hand):
        self.rank = RANK_ONE_PAIR
        self.high_card = high_card_pairs(hand)
        self.hand = hand 

    @property    
    def match_found(self):
        return number_of_unique_values(self.hand) == VALUES_ONE_PAIR

class HighCard(object):     
    '''recursive highest value wins 
    RANK 10 '''
    def __init__(self, hand):
        self.rank = RANK_HIGH_CARD
        self.high_card = highcard_weighted_card_value_sum(hand)
        self.hand = hand 

    @property
    def match_found(self):
        return not(number_of_suits(self.hand) == SUITS_FLUSH ) and (number_of_unique_values(self.hand) == VALUES_HIGH_CARD) and not(is_straight(self.hand))

@autocomplete_ordering
class PokerHand(object):
    POKER_HANDS=[RoyalFlush, StraightFlush, FourOfAKind, FullHouse, Flush,\
                 Straight, ThreeOfAKind, TwoPairs, OnePair, HighCard]          

    def __repr__(self):  return self.hand

    def __init__(self, hand):
        self.hand = hand
        self._ranking = ()

    @property
    def ranking(self):
        self._get_ranking()
        return self._ranking 

    def _get_ranking(self):
        for hand_type in self.POKER_HANDS:
            handle_hand = hand_type(self.hand)
            if not handle_hand.match_found:
                continue     
            self._ranking=(handle_hand.rank, -handle_hand.high_card)    
    
    def __lt__(self,other):
       return self.ranking < other.ranking

    def __eq__(self,other):
       return self.ranking == other.ranking
