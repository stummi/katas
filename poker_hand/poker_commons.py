from poker_constants import *

def suits(hand):
    return hand[SUITS_START::OFFSET].strip()

def values(hand):
    return hand[VALUES_START::OFFSET].strip()

def number_of_suits(hand):
    return len(set(suits(hand)))

def number_of_unique_values(hand):
    return len(set(values(hand)))

def card_value(card):
   return (CARD_RANKS.index(card) + 1)

def high_card_pairs(hand):
    high_card = 0
    not_pair_cards=[]
    for value in sorted_value_string(hand):
        if sorted_value_string(hand).count(value) == PAIR :
            high_card += card_value(value)*FACTOR
        not_pair_cards.append(value)     
    high_card += card_value_sum(not_pair_cards)
    return high_card


def high_card_multiple_of_a_kind(hand, how_many_of_a_kind=ZERO):
        high_card = ZERO 
        for value in sorted_value_string(hand):
            if sorted_value_string(hand).count(value) == how_many_of_a_kind:
               high_card += card_value(value) 
        return high_card

def card_value_sum(cards):
    value_sum = ZERO
    for card_index, card in enumerate(sorted(cards)):
        value_sum += card_value(card)*(card_index + 1)
    return value_sum

def highcard_weighted_card_value_sum(hand):
    card_values=list(map(card_value,sorted_values(hand)))
    card_value_sum = ZERO
    for card_index, value_of_card in enumerate(card_values):
        card_value_sum += value_of_card ** ( card_index + 1 )
    return card_value_sum

def is_royal(hand): 
       return sorted([ACE, KING, QUEEN, JACK, TEN]) == sorted(values(hand))

def sorted_value_string(hand):
    return ''.join(sorted(values(hand)))

def sorted_values(hand):
    return sorted(values(hand))

def map_ace_to_beginning(sorted_hand):
     if (ACE in sorted_hand) and not (KING in sorted_hand):
         return ACE+sorted_hand[:SKIP_LAST_CARD]
     return sorted_hand    

def is_straight(hand):
    if (map_ace_to_beginning(sorted_value_string(hand)) in FULL_STRAIGHT) and number_of_unique_values(hand) == VALUES_STRAIGHT:
        return True
    return False

def is_multiple_cards_of_one_kind(hand, number_of_cards):
    for value in sorted_value_string(hand):
        if sorted_value_string(hand).count(value) == number_of_cards :
            return True