import numpy as np

WATER=0
SHIPS=1
WIDTH=10
TOTAL_CELLS = WIDTH*WIDTH
NO_BATTLESHIPS = 1
NO_CRUISERS = 2
NO_DESTROYERS = 3
NO_SUBMARINES = 4

BATTLESHIP = 4
CRUISER = 3
DESTROYER = 2
SUBMARINE = 1

SHIP_CELLS = NO_BATTLESHIPS*BATTLESHIP + NO_CRUISERS*CRUISER + NO_DESTROYERS*DESTROYER + NO_SUBMARINES*SUBMARINE
WATER_CELLS = TOTAL_CELLS - SHIP_CELLS

def turn_battle_field(field):
   return map(list, zip(*field))

def count_all_values(field, value):
   return sum(count_values_per_row(field, value))

def count_values_per_row(field, value):
   return [row.count(value) for row in field]

def count_values_per_column(field, value):
   return [row.count(value) for row in turn_battle_field(field)]

def validate_amount_of_water(field):
    return count_all_values(field, WATER) == WATER_CELLS

def validate_amount_of_ships(field):
    return count_all_values(field, SHIPS) == SHIP_CELLS

def count_conseq_ones(vector):
   all=0
   for cell in vector:
      if cell == 1: all+=1
      if cell == 0: return all
   return all


def remove_ship(field, shiptype, offset):
    '''remove ship starting from first encountered
       ship cell in the first try (offset == 0), move
       by offset(==try number) in later trials
       first try to remove it from rows, if this fails try columns'''
    newfield = np.zeros(field.shape)
    # try horizontally
    for irow in range(field.shape[0]):
      for icolumn, cell in enumerate(field[irow,:]):
         if sum(field[irow,icolumn:icolumn+shiptype]) >=  shiptype:
            if count_conseq_ones(field[irow, icolumn:]) > shiptype:
               newfield[irow,icolumn+offset:shiptype+offset] = cell 
            else:
               newfield[irow,icolumn:icolumn+shiptype] = cell
            break
      else:
         continue
      break

    if np.sum(newfield) == WATER:
       # try vertically
       for icolumn in range(field.shape[1]):
           for irow, cell in enumerate(field[:,icolumn]):
               if sum(field[irow:irow+shiptype, icolumn]) >= shiptype:
                   if count_conseq_ones(field[irow:,icolumn]) > shiptype:
                     newfield[irow+offset:irow+offset+shiptype, icolumn] = cell
                   else:
                     newfield[irow:irow+shiptype,icolumn] = cell
                   break
           else:
              continue
           break
    return np.subtract(field,newfield), np.sum((newfield))           

def remove_battleship(field, shiptype, no_try):
      cleared_field, no_ships = remove_ship(field, shiptype, no_try)
      if no_ships == shiptype:
         return cleared_field, True
      else:
         return cleared_field, False

def remove_all_ships(field):
   '''remove all battle ship types from battlefield
   in recursive trials --> refactor to recursion'''
   for itry_battleship in range(WIDTH-BATTLESHIP):
      field, is_removed=remove_battleship(field, BATTLESHIP, itry_battleship)
      if is_removed:
        for itry_cruiser in range(WIDTH-CRUISER):
            field, is_removed = remove_battleship(field,CRUISER, itry_cruiser)
            if is_removed:
               for itry_destroyer in range(WIDTH-DESTROYER): 
                   field, is_removed = remove_battleship(field, DESTROYER, itry_destroyer)
                   if is_removed:
                      field, is_removed = remove_battleship(field, SUBMARINE, 0)
                   if is_removed:
                      break
            if is_removed:
                break
      if is_removed:
        return is_removed
    return False  

def validate_battlefield(battleField):
      return validate_amount_of_ships(battleField) and validate_amount_of_water(battleField) and remove_all_ships(np.array(battleField))

if __name__ == "__main__":
  print("import ..")
    


     
